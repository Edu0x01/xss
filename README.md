# XSS

XSS is a simple shell tool developed to test Cross-Site Scripting (XSS) vulnerabilities on websites. This tool is useful for information security professionals, security researchers, and ethical hacking enthusiasts who want to identify and exploit XSS vulnerabilities in their web applications.

## How It Works

XSS uses a list of known payloads to exploit XSS vulnerabilities. It injects these payloads into URL parameters and checks if the web application is vulnerable to XSS based on server responses.

## How to Use

1. Clone the repository to your local environment:

```bash
git clone https://gitlab.com/Edu0x01/xss.git
```

2. Navigate to the project directory:

```bash
cd xss
```

3. Ensure that the `xss.txt` file is present and contains a comprehensive list of XSS payloads.

4. To run the .sh file you will have to do the following:

```bash
chmod +x xss.sh
```

5. Run the script by providing the URL of the site you want to test:

```bash
./xss.sh https://example.com/page?param=
```

## Future Updates

This project is in the testing phase and is subject to ongoing improvements. The following updates are planned:

- Enhancements to XSS detection effectiveness.
- Support for other types of web vulnerabilities.
- Improvements in usability and interface.

## Extra

**If you find this project useful, I would really appreciate supporting me by giving this repository a star or buying me a coffee.**

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/edu0x01)

Copyright © 2023, _Edu0x01_
